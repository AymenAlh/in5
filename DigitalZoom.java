import java.util.Scanner;

public class DigitalZoom {
    public static void main(String[] args){
        Scanner obj = new Scanner(System.in);

        System.out.println("Ange namn på filen, zoomfaktor och centerpunkten mellan 0 och 1");
        System.out.print("(exemple.jpg zoomfaktor x-centerpunkten y-centerpunkten): ");

        String input = obj.nextLine();

        String[] inputSplit = input.split(" ");

        Picture pic = new Picture(inputSplit[0]);
        double zoomX = Double.parseDouble(inputSplit[1]);
        double centerX = Double.parseDouble(inputSplit[2]);
        double centerY = Double.parseDouble(inputSplit[3]);


        if(zoomX < 1.0){
            zoomX = 1 - zoomX;
        }
        else zoomX = 1.0;

        double picW = pic.width()* (zoomX+1);
        double picH = pic.height()* (zoomX+1);

        System.out.println(pic.width() + " " + pic.height());
        System.out.print(picW + " " + picH);



    }
}
